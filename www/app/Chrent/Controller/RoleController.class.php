<?php
// +----------------------------------------------------------------------
// | 角色管理控制器
// +----------------------------------------------------------------------
// | 深圳市君鉴测试仪器租赁有限公司
// +----------------------------------------------------------------------
// | Author: 卓战友 125323228@qq.com
// +----------------------------------------------------------------------
namespace Chrent\Controller;
use Think\Controller;
use Common\Controller\Tree;
class RoleController extends ChrentController {
    public function index()
	{
		if(!IS_AJAX)
		{
			$this->display();
		}
		else
		{
			$menu_name      =   trim(I('get.menu_name'));
			if($menu_name) $map['menu_name'] = array('like',"%{$menu_name}%");
			$m = M('menu')->field('mid id,menu_name,url,ico,sort_order,position')->where($map);
			$data = $this->jqgrid($m);
			$arr = $data->rows;
			foreach($arr as $k=>&$cell)
			{
				$v = $cell['cell'];
				$remove_url = U("remove","id=" . $v['id']);
				$v['op']         = "<a href=\"javascript:lay('" . U('edit','id=' . $v['id']) . "','编辑菜单信息',400,30);\">修改</a> | <a href=\"javascript:del('确定要删除吗?','$remove_url',loadData);\">删除</a>";
				$arr[$k]['cell'] = $v;
			}
			$data->rows = $arr;
			echo json_encode($data);
		}
    }
    public function add()
	{
        if(IS_POST){
			$mod = D('Menu');
			$data = I('post.data');
			if (!$mod->create($data)){
				$this->error($mod->getError());
			}else{
				if($mod->add($data))
				{
					$this->success('操作成功！', U('index'));
				}
				else
				{
					$this->error('数据写入失败!');
				}
			}
        } else {
			$menu = M('Menu')->field('mid,pid,menu_name,url,ico')->where($where)->order('sort_order asc')->select();
			$tree = new Tree();
			$tree->setData($menu);
			$tree->option();
			$pid = $tree->menuStr;
			$this->assign('pid',$pid);
			$this->assign('position',array(array('code'=>1,'name'=>'后台菜单')));
			$this->display('Menu:form');
		}
    }
    public function edit($id)
	{
        if(IS_POST){
			$mod = D('Menu');
			$data = I('post.data');
			$id   = I('post.id');
			if (!$mod->create($data)){
				$this->error($mod->getError());
			}else{
				if($mod->where("mid=$id")->data($data)->save())
				{
					$this->success('操作成功！', U('index'));
				}
				else
				{
					$this->error('数据写入失败!');
				}
			}
        }else{
			$map = array('mid'=>$id);
			$data = M('Menu')->where($map)->find();
			$menu = M('Menu')->field('mid,pid,menu_name,url,ico')->where($where)->order('sort_order asc')->select();
			$tree = new Tree();
			$tree->setData($menu);
			$tree->option(0,$data['pid']);
			$pid = $tree->menuStr;
			$this->assign('pid',$pid);
			$this->assign('data',$data);
			$this->assign('position',array(array('code'=>1,'name'=>'后台菜单')));
			$this->display('Menu:form');
		}
	}
	function remove($id)
	{
		$mod = M('Menu');
		if($mod->delete($id))
		{
			$this->success('操作成功！', U('index'));
		}
		else
		{
			$this->error('数据操作失败!');
		}
	}
}