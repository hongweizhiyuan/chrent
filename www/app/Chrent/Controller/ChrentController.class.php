<?php
// +----------------------------------------------------------------------
// | 系统基础控制器
// +----------------------------------------------------------------------
// | 深圳市君鉴测试仪器租赁有限公司
// +----------------------------------------------------------------------
// | Author: 卓战友 125323228@qq.com
// +----------------------------------------------------------------------

namespace Chrent\Controller;
use Think\Controller;
class ChrentController extends Controller {

    /**
     * 系统基础控制器初始化
     */
    protected function _initialize(){
        // 获取当前用户ID
        define('UID',is_login());
        if( !UID ){// 还没登录 跳转到登录页面
            $this->redirect('Public/login');
        }
        /* 读取数据库中的配置 */
        $config =   S('DB_CONFIG_DATA');
        if(!$config){
            $config	=	D('Config')->lists();
            S('DB_CONFIG_DATA',$config);
        }
        C($config);
        // 是否是超级管理员
        define('IS_ROOT',   is_administrator());
        if(!IS_ROOT && C('ADMIN_ALLOW_IP')){
            // 检查IP地址访问
            if(!in_array(get_client_ip(),explode(',',C('ADMIN_ALLOW_IP')))){
                $this->error('403:禁止访问');
            }
        }
        // 检测访问权限
        $access =   $this->accessControl();
        if ( $access === false ) {
            $this->error('403:禁止访问');
        }elseif( $access === null ){
			//检测非动态权限
			$rule  = strtolower(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME);
			if ( !$this->checkRule($rule,array('in','1,2')) ){
				$this->error('未授权访问!');
			}
        }
        $this->assign('__MENU__', $this->chrentMenus());
		$this->assign('__TIME__', time());
    }
    /**
     * action访问控制,在 **登陆成功** 后执行的第一项权限检测任务
     *
     * @return boolean|null  返回值必须使用 `===` 进行判断
     *
     *   返回 **false**, 不允许任何人访问(超管除外)
     *   返回 **true**, 允许任何管理员访问,无需执行节点权限检测
     *   返回 **null**, 需要继续执行节点权限检测决定是否允许访问
     * @author 卓战友
     */
    final protected function accessControl(){
        if(IS_ROOT){
            return true;//管理员允许访问任何页面
        }
		$allow = C('ALLOW_VISIT');
		$deny  = C('DENY_VISIT');
		$check = strtolower(CONTROLLER_NAME.'/'.ACTION_NAME);
        if ( !empty($deny)  && in_array_case($check,$deny) ) {
            return false;//非超管禁止访问deny中的方法
        }
        if ( !empty($allow) && in_array_case($check,$allow) ) {
            return true;
        }
        return null;//需要检测节点权限
    }
    /**
     * 权限检测
     * @param string  $rule    检测的规则
     * @param string  $mode    check模式
     * @return boolean
     * @author 卓战友
     */
    final protected function checkRule($rule, $type=1, $mode='url'){
        if(IS_ROOT){
            return true;//管理员允许访问任何页面
        }
        static $Auth    =   null;
        if (!$Auth) {
            $Auth       =   new \Common\Controller\Auth();
        }
        if(!$Auth->check($rule,UID,$type,$mode)){
            return false;
        }
        return true;
    }
    /**
     * 获取控制器菜单数组,二级菜单元素位于一级菜单的'_child'元素中
     * @author 卓战友
     */
    final public function chrentMenus(){
		$menus  =   session('ADMIN_MENU_LIST'.MODULE_NAME);
		if(empty($menus))
		{
			$where['position']   =   1;
			$menu = M('Menu')->field('mid,pid,menu_name,url,ico')->where($where)->order('sort_order asc')->select();
			$tree = new \Common\Controller\Tree();
			$tree->setData($menu);
			$tree->chrent_menu();
			$menus = $tree->menuStr;
            session('ADMIN_MENU_LIST'.MODULE_NAME,$menus);
        }
        return $menus;
	}
    /**
     * 获取使用jqgrid的数据
     *
     * @param  sting|Model  $model   模型名或模型实例
     * @author 卓战友
     *
     * @return array
     * 返回数据集
     */
	function jqgrid($model)
	{
		if(!strtolower(substr($model,0,6))=='select' && is_string($model))
		{
			$model  =   M($model);
		}
		$sql = is_object($model)?$model->select(false):$model;
		
		$page  = I('request.page')?I('request.page'):1; 
		$limit = I('request.rows')?I('request.rows'):50;
		$sidx  = I('request.sidx');
		$sord  = I('request.sord');
		$m = M();
		$total_sql = "select count(*) num from ($sql) a";
		$tmp = $m->query($total_sql);
		$count = $tmp[0]['num'];
		$total_pages = $count>0?ceil($count/$limit):0;
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;
		$start = $start<0?0:$start;
		$sql = $sql . " ORDER BY $sidx $sord LIMIT $start , $limit";;
		$data = $m->query($sql);
		$responce = new \stdClass();
		$responce->page = $page; 
		$responce->total = $total_pages; 
		$responce->records = $count;
		$responce->rows = array();
		if(is_array($data))
		{
			foreach($data as $i=>$row)
			{ 
				$responce->rows[$i]['id']   = $row['id']; 
				$responce->rows[$i]['cell'] = $row;
			}
		}
		return $responce;
	} 
}