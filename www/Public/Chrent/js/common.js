//dom加载完成后执行的js
;$(function(){
    // 独立域表单获取焦点样式
    $(".text").focus(function(){
        $(this).addClass("focus");
    }).blur(function(){
        $(this).removeClass('focus');
    });
    $("textarea").focus(function(){
        $(this).closest(".textarea").addClass("focus");
    }).blur(function(){
        $(this).closest(".textarea").removeClass("focus");
    });
	$("a").bind("focus",function(){if(this.blur){this.blur();}});
});
// 本地时钟
function clockon() {
	var now = new Date();
	var year = now.getFullYear(); // getFullYear getYear
	var month = now.getMonth();
	var date = now.getDate();
	var day = now.getDay();
	var hour = now.getHours();
	var minu = now.getMinutes();
	var sec = now.getSeconds();
	var week;
	month = month + 1;
	if (month < 10)
		month = "0" + month;
	if (date < 10)
		date = "0" + date;
	if (hour < 10)
		hour = "0" + hour;
	if (minu < 10)
		minu = "0" + minu;
	if (sec < 10)
		sec = "0" + sec;
	var arr_week = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");
	week = arr_week[day];
	var time = "";
	time = year + "年" + month + "月" + date + "日" + " " + hour + ":" + minu
			+ ":" + sec + " " + week;

	$("#time").html(time);
	setTimeout("clockon()", 200);
}
function del(msg,url,callback)
{
	$.layer({
		shade: [0],
		area: ['auto','auto'],
		dialog: {
			msg: msg,
			btns: 2,                    
			type: 8,
			btn: ['确定','取消'],
			yes: function()
			{
				$.ajax({
					async: false,
					type : "GET",
					url : url,
					success : function(data) {
						if(data.status){
							callback();
							layer.closeAll();
						} else {
							myAlert(data.info);
						}
					}
				});
			}
		}
	});
}
